pytaVSL
=======

pytaVSL stands for python tiny alpproximative VSL.
It's a VJing and lights-projector virtualization program using pi3d, and aimed to reproduce some of the features one could find in the pd-patches set called VSL. The final destination arch of pytaVSL is the Raspberry PI.

Dependencies
============

- python-liblo
- pi3d

License
=========

Copyleft © 2018 Plagiat Brother; Original Code © Aurélien Roux; based upon Virtual Stage Light by Gregory David.
This program is a free software released under the [GNU/GPL3](https://github.com/PlagiatBros/pytaVSL/blob/master/LICENSE) license.

Included fonts: leaguegothic (sans), freemono (mono)
